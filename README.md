# Product Name
RocketApp

Swift Project "RocketApp" provides us  iOS  sample which is using `MVVM` pattern for making some API call and showing the response on the view.
For reactive part is used `RxSwift` and `Swinject` for making injections(libraries are added with carthage)
For unit tests is used `Factory` library( added in the project with SPM)

### Networking
* `URLSession` native

### Usage
* Clone the repository or download the zip and run
