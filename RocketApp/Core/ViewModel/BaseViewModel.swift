//
//  BaseViewModel.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public protocol ViewModelling {
    var error: BehaviorRelay<Error?> { get }
}

open class BaseViewModel<T>: NSObject, ViewModelling {
    public let inputs: T

    public let disposeBag = DisposeBag()
    public let error = BehaviorRelay<Error?>(value: nil)

    public init(inputs: T) {
        self.inputs = inputs
    }

    open func handleEvent<R>(_ event: Event<R>, dataHandler: (R) -> Void) {
        switch event {
        case .next(let response):
            if let error = handleSuccess(response) {
                self.error.accept(error)
            } else {
                dataHandler(response)
            }
        case .error(let error):
            handleError(error)
        case .completed:
            break
        }
    }

    open func handleSuccess(_ data: Any) -> Error? {
        return nil
    }

    open func handleError(_ error: Error) {
        self.error.accept(self.createError(with: error))
    }

    open func createError(with error: Error) -> Error? {
        return ViewError.message(error.localizedDescription)
    }
}



