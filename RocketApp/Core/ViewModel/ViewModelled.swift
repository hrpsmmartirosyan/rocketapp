//
//  ViewModelled.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public protocol ViewModelled {
    associatedtype T: ViewModelling

    var viewModel: T? { get }

    func bindViewModel(_ viewModel: T)
}

