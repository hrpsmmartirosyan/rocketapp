//
//  ErrorHandler.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import UIKit

public enum ServiceError: Error {
    case unexpectedError
}

public enum ViewError: Error {
    case message(String)
}

public class ErrorHandler {
    public func handleError(_ error: Error) -> UIAlertController? {
        let alert = UIAlertController(title: "network.error.title".localized, message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "error.message.ok.title".localized, style: .default, handler: nil))

        return alert
    }
}
