//
//  BaseViewController.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//
//
import UIKit
import RxSwift

open class BaseViewController<T: ViewModelling>: UIViewController, ViewModelled {
    open var viewModel: T?
    lazy open var errorHandler = ErrorHandler()
    lazy open var disposeBag = DisposeBag()

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        if let vm = self.viewModel {
            self.bindViewModel(vm)
            self.bindError(vm)
        }
    }

    open func bindViewModel(_ viewModel: T) {}

    open func bindError(_ viewModel: T) {
        viewModel.error
            .asDriver()
            .drive(onNext: { [weak self] error in
                guard let error = error else { return }

                self?.handleError(error)
            })
            .disposed(by: disposeBag)
    }


    open func handleError(_ error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            guard let errorMessage = self.errorHandler.handleError(error) else { return }

            self.present(errorMessage, animated: true, completion: nil)
        }
    }
}

