//
//  DataFetchingManager.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

class DataFetchingManager {
    //Network manager to fetch data from data source
    //Will be injected with constructor injection
    let networkManager: NetworkManager

    //Parser to pars data from data source
    //Will be injected with constructor injection
    let parser: Parser

    //Used to add all disposables to dispose them after object dealocation
    lazy var disposeBag = DisposeBag()

    init(with networkManager: NetworkManager, parser: Parser) {
        self.networkManager = networkManager
        self.parser = parser
    }

    func execute<T: Decodable, R: Routing, E: Decodable>(_ route: R, errorType: E.Type) -> Observable<T> where E: ServiceErrorHandling {
        return Observable.create { observer in
            self.networkManager
                .fetch(route)
                .asObservable()
                .subscribe(onNext: { [weak self] data in
                    guard let `self` = self else { return }

                    let jsonDecoder = JSONDecoder()

                    do {
                        let decodedLog = try jsonDecoder.decode(ErrorResponse.self, from: data)

                        guard let error = decodedLog.handle() else { return }

                        observer.onError(error)
                        observer.on(.completed)
                    } catch {
                        self.parser.parse(data: data)
                            .asObservable()
                            .subscribe(onNext: { entity in
                                observer.onNext(entity)
                            }, onError: { error in
                                observer.onError(error)
                            })
                            .disposed(by: self.disposeBag)
                    }
                    }, onError: { error in
                        observer.onError(error)
                })

                .disposed(by: self.disposeBag)

            return Disposables.create()
        }
    }
}



