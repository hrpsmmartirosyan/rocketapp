//
//  NetworkManager.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

class NetworkManager {
    init() {}

    func fetch<R: Routing>(_ routing: R) -> Observable<Data> {
        return Observable.create { observer in

            // set up the session
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)

            // make the request
            if let urlRequest = routing.urlRequest {
                let task = session.dataTask(with: urlRequest) {
                    (data, response, errorResponse) in
                    // check for any errors
                    if let httpResponse = response as? HTTPURLResponse {
                        if !(200...299).contains(httpResponse.statusCode) {
                            observer.onError(ServiceError.unexpectedError)
                        }
                    }
                    guard errorResponse == nil else {
                        observer.onError(errorResponse!)
                        return
                    }

                    guard let responseData = data else {
                        #if DEV
                        print("Data does not exist")
                        #endif

                        return
                    }

                    observer.onNext(responseData)
                    observer.on(.completed)
                }
                task.resume()
            }
            return Disposables.create()
        }
    }
}

