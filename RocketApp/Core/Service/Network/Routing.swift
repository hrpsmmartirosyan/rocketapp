//
//  Routing.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

protocol Routing {

    // Base url
    var baseURLString: String { get }
    var method: RequestType { get }
    var routPath: String { get }
    var pathValues: [String]? { get }
    var parameters: [String: Any]? { get }
    var encoding: ParameterEncoding { get }
    var headers: [String: String]? { get }
    var urlRequest: URLRequest? { get }
}

extension Routing {
    var baseURLString: String {
        return "https://api.spacexdata.com/v4"
    }

    var method: RequestType {
        return .POST
    }

    var routPath: String {
        return ""
    }

    var parameters: [String: Any]? {
        return nil
    }

    var pathValues: [String]? { nil }

    var encoding: ParameterEncoding {
        return ParameterEncoding.json
    }

    var headers: [String: String]? {
        return nil
    }

    var urlRequest: URLRequest? {
        let baseURLStirng = baseURLString

        guard var url = URL(string: baseURLStirng) else {
            #if DEV
            print("cannot create URL")
            #endif

            return nil
        }

        if !routPath.isEmpty {
            url.appendPathComponent(routPath)
        }

        if let pathValues = pathValues {
            for value in pathValues {
                url.appendPathComponent(value)
            }
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        if let headers = self.headers {
            for (key, value) in headers {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
        }

        if let parameters = self.parameters {
            do {
                urlRequest = try encoding.encode(request: urlRequest, parameters: parameters)
            } catch {
                #if DEV
                print("parameters encoding issue")
                #endif
            }
        }

        return urlRequest
    }
}

