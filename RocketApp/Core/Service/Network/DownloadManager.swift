//
//  DownloadManager.swift
//  RocketApp
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift


protocol DownloadManagerServicing {
    func download(from url: URL) -> Observable<(Data?, Error?)>
    func cancelRequest(of url: URL)
}

final class DownloadManager: DownloadManagerServicing {
    typealias PostDownloadAction = (Data?, Error?) -> Void

    private let queue = DispatchQueue(label: "DownloadManagerQueueId")

    public init() {}

    private var cache: [URL: Data] = [:]
    private var dataRequest: [URL: URLSessionDataTask] = [:]
    private var postDownloadActions: [URL: [PostDownloadAction]] = [:]

    public func download(from url: URL) -> Observable<(Data?, Error?)> {
        return Observable.create({ [weak self] observer -> Disposable in
            self?.queue.sync {
                if let data = self?.cache[url] {
                    print("Data returned from cache")
                    observer.onNext((data, nil))

                    return
                }

                // if this statement is true it means we already have a data task in process, so we add case in postDownloadActions and run the actions when the process of downlaoding is done
                if self?.dataRequest[url] != nil {
                    var actions: [PostDownloadAction] = self?.postDownloadActions[url] ?? []
                    let action: PostDownloadAction = { data, error in
                        observer.onNext((data, error))
                        observer.onCompleted()
                    }

                    actions.append(action)

                    self?.postDownloadActions[url] = actions

                    return
                }

                let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    if let data = data {
                        self?.cache[url] = data
                    } else {
                        print("Downloaing failed \(error?.localizedDescription ?? "")")
                    }

                    observer.onNext((data, error))
                    observer.onCompleted()

                    self?.dataRequest[url] = nil

                    self?.postDownloadActions[url]?.forEach({ action in
                        action(data, error)
                    })

                    self?.postDownloadActions[url] = nil
                })

                dataTask.resume()

                self?.dataRequest[url] = dataTask
            }

            return Disposables.create()
        })
    }

    public func cancelRequest(of url: URL) {
        self.queue.sync { [weak self] in
            self?.dataRequest[url]?.cancel()
            self?.dataRequest[url] = nil
        }
    }
}


