//
//  Parser.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

struct Parser {
    func parse<T: Decodable>(data: Data) -> Observable<T> {
        return Observable.create { observer in
            let jsonDecoder = JSONDecoder()

            do {
                let decodedLog = try jsonDecoder.decode(T.self, from: data)

                observer.onNext(decodedLog)
                observer.on(.completed)

            } catch {
                observer.onError(error)
            }

            return Disposables.create()
        }
    }
}

