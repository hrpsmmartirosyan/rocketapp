//
//  Patch.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class Patch: Codable {
    public let smallImageUrl: String?
    public let largeImageUrl: String?

    public init(smallImageUrl: String?, largeImageUrl: String?) {
        self.smallImageUrl = smallImageUrl
        self.largeImageUrl = largeImageUrl
    }

    private enum CodingKeys: String, CodingKey {
        case smallImageUrl = "small"
        case largeImageUrl = "large"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.smallImageUrl = try? values.decodeIfPresent(String.self, forKey: .smallImageUrl)
        self.largeImageUrl = try? values.decodeIfPresent(String.self, forKey: .largeImageUrl)
    }
}

extension Patch: Equatable {
    //for using in unit tests
    public static func == (lhs: Patch, rhs: Patch) -> Bool {
        return lhs.smallImageUrl == rhs.smallImageUrl && lhs.largeImageUrl == rhs.largeImageUrl
    }
}


