//
//  Rocket.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class Rocket: Codable {
    public let name: String
    public let dateString: String?
    public let details: String?
    public let patch: Patch?
    public let landpadData: [LandpadData]?

    public init(name: String, dateString: String?, details: String?, patch: Patch?, landpadData: [LandpadData]?) {
        self.name = name
        self.dateString = dateString
        self.details = details
        self.patch = patch
        self.landpadData = landpadData
    }

    enum PatchCodingKeys: String, CodingKey {
        case links = "patch"
    }

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case dateString = "date_utc"
        case details = "details"
        case patch = "links"
        case landpadData = "cores"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try values.decode(String.self, forKey: .name)
        self.dateString = try? values.decodeIfPresent(String.self, forKey: .dateString)
        self.details = try? values.decodeIfPresent(String.self, forKey: .details)
        self.landpadData = try? values.decodeIfPresent([LandpadData].self, forKey: .landpadData)

        let patchContainer = try values.nestedContainer(keyedBy: PatchCodingKeys.self, forKey: .patch)
        self.patch = try? patchContainer.decodeIfPresent(Patch.self, forKey: .links)
    }
}

