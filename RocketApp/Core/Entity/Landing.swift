//
//  Landing.swift
//  RocketApp
//
//  Created by hripsimem on 1/18/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class Landing: Codable {
    public let locality: String
    public let region: String?

    public init(locality: String, region: String?) {
        self.locality = locality
        self.region = region
    }

    private enum CodingKeys: String, CodingKey {
        case locality = "locality"
        case region = "region"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.locality = try values.decode(String.self, forKey: .locality)
        self.region = try? values.decodeIfPresent(String.self, forKey: .region)
    }
}


