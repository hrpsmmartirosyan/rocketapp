//
//  ErrorResponse.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

protocol ServiceErrorHandling {
    func handle() -> Error?
}

//This class is created in case if server will send the error as an parsable object
public final class ErrorResponse: Codable {
    public let statusCode: Int?
    public let errorCode: Int?
    public let message: String

    public init(statusCode: Int?, errorCode: Int?, message: String) {
        self.statusCode = statusCode
        self.message = message
        self.errorCode = errorCode
    }
}

extension ErrorResponse: ServiceErrorHandling {
    func handle() -> Error? {
        if let errorCode = self.errorCode, errorCode != 0  {
            //for now only unexpectedError error case is exist but there is possibility to have more types
            return ServiceError.unexpectedError
        }
        return nil
    }
}

