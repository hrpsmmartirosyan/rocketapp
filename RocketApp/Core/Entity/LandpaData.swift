//
//  LandpadData.swift
//  RocketApp
//
//  Created by hripsimem on 1/18/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class LandpadData: Codable {
    public let landpad: String?

    public init(landpad: String?) {
        self.landpad = landpad
    }

    private enum CodingKeys: String, CodingKey {
        case landpad = "landpad"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.landpad = try? values.decodeIfPresent(String.self, forKey: .landpad)
    }
}

extension LandpadData: Equatable {
    public static func == (lhs: LandpadData, rhs: LandpadData) -> Bool {
        return lhs.landpad == rhs.landpad
    }
}



