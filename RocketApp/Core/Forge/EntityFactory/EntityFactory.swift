//
//  EntityFactory.swift
//  RocketApp
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import Fakery

public class EntityFactory {
    public static let faker = Faker(locale: "en") // custom "en" because I don't want to embed all json of Fakery into Forge

    private static let jsonEncoder = JSONEncoder()

    static func json<T: Encodable>(from value: T) -> Data {
        return (try? jsonEncoder.encode(value)) ?? Data()
    }
}

extension Fakery.Faker.Lorem {
    public func wordsArray(amount: Int) -> [String] {
        return self.words(amount: amount).components(separatedBy: " ")
    }

    public func uniqueWordsArray(amount: Int) -> [String] {
        var wordArray = [String]()
        repeat {
            let word = self.word()
            if !wordArray.contains(word) {
                wordArray.append(word)
            }
        } while wordArray.count < amount
        return wordArray
    }
}

extension Fakery.Faker.Internet {
    public func realUrl() -> URL {
        return URL(string: self.url())!
    }
}


