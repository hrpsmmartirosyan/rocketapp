//
//  PatchFactory.swift
//  RocketApp
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class PatchFactory: EntityFactory {

    public static let smallImageUrl = faker.lorem.word()
    public static let largeImageUrl = faker.lorem.word()

    public static func create(smallImageUrl: String = faker.lorem.word(),
                              largeImageUrl: String = faker.lorem.word()) -> Patch {

        return Patch(smallImageUrl: smallImageUrl, largeImageUrl: largeImageUrl)
    }

    public static func create(count: Int) -> [Patch] {
        var result = [Patch]()
        for _ in 0..<count {
            result.append(self.create())
        }

        return result
    }

    public static func createTestableData(smallImageUrl: String = smallImageUrl,
                                          largeImageUrl: String = largeImageUrl) -> Data {
        let object = Patch(smallImageUrl: smallImageUrl, largeImageUrl: largeImageUrl)
        return json(from: object)
    }

    public static func createInvalid(smallImageUrl: Bool = false, largeImageUrl: Bool = false) -> Data {
        var json = jsonData

        if smallImageUrl {
            json["small"] = nil
        }

        if largeImageUrl {
            json["larg"] = nil
        }

        do {
            return try JSONSerialization.data(withJSONObject: json)
        } catch {
            fatalError("couldn't create json data : \(error)")
        }
    }

    public static let jsonObject = Patch(
        smallImageUrl: smallImageUrl,
        largeImageUrl: largeImageUrl)

    public static let jsonObjects: [Patch] = .init(repeating: jsonObject, count: 3)

    public static let jsonData: [String: Any] = [
        "small": smallImageUrl,
        "larg": largeImageUrl
    ]
    public static let jsonArrayData: [[String: Any]] = .init(repeating: jsonData, count: 3)
}


