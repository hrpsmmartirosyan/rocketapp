//
//  LandpadDataFactory.swift
//  RocketApp
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class LandpadDataFactory: EntityFactory {

    public static let landpad = faker.lorem.word()

    public static func create(landpad: String = faker.lorem.word()) -> LandpadData {
        return LandpadData(landpad: landpad)
    }

    public static func create(count: Int) -> [LandpadData] {
        var result = [LandpadData]()
        for _ in 0..<count {
            result.append(self.create())
        }

        return result
    }

    public static func createTestableData(landpad: String = landpad) -> Data {
        let object = LandpadData(landpad: landpad)
        return json(from: object)
    }

    public static func createInvalid(landpad: Bool = false) -> Data {
        var json = jsonData

        if landpad {
            json["landpad"] = nil
        }

        do {
            return try JSONSerialization.data(withJSONObject: json)
        } catch {
            fatalError("couldn't create json data : \(error)")
        }
    }

    public static let jsonObject = LandpadData(
        landpad: landpad)

    public static let jsonObjects: [LandpadData] = .init(repeating: jsonObject, count: 3)

    public static let jsonData: [String: Any] = [
        "landpad": landpad
    ]
    public static let jsonArrayData: [[String: Any]] = .init(repeating: jsonData, count: 3)
}



