//
//  RocketFactory.swift
//  RocketApp
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public final class RocketFactory: EntityFactory {

    public static let name = faker.lorem.word()
    public static let dateString = faker.lorem.word()
    public static let details = faker.lorem.word()
    public static let patch = PatchFactory.jsonData
    public static let landpadData = [LandpadDataFactory.jsonData]

    public static func create(name: String = faker.lorem.word(),
                              dateString: String = faker.lorem.word(),
                              details: String = faker.lorem.word(),
                              patch: Patch = PatchFactory.create(),
                              landpadData: [LandpadData] = [LandpadDataFactory.create()]) -> Rocket {

        return Rocket(name: name, dateString: dateString, details: details, patch: patch, landpadData: landpadData)
    }

    public static func create(count: Int) -> [Rocket] {
        var result = [Rocket]()
        for _ in 0..<count {
            result.append(self.create())
        }

        return result
    }

    public static func createTestableData(name: String = name,
                                          dateString: String = dateString,
                                          details: String = details,
                                          patch: Patch = PatchFactory.jsonObject,
                                          landpadData: [LandpadData] = [LandpadDataFactory.jsonObject]) -> Data {
        let object = Rocket(name: name, dateString: dateString, details: details, patch: patch, landpadData: landpadData)
        return json(from: object)
    }

    public static func createInvalid(name: Bool = false, dateString: Bool = false, details: Bool = false, patch: Bool = false, landpadData: Bool = false) -> Data {
        var json = jsonData

        if name {
            json["name"] = nil
        }

        if details {
            json["details"] = nil
        }

        if dateString {
            json["dateString"] = nil
        }

        if patch {
            json["links"] = nil
        }

        if landpadData {
            json["cores"] = nil
        }

        do {
            return try JSONSerialization.data(withJSONObject: json)
        } catch {
            fatalError("couldn't create json data : \(error)")
        }
    }

    public static let jsonObject = Rocket(
        name: name,
        dateString: dateString,
        details: details,
        patch: PatchFactory.jsonObject,
        landpadData: [LandpadDataFactory.jsonObject])

    public static let jsonObjects: [Rocket] = .init(repeating: jsonObject, count: 3)

    public static let jsonData: [String: Any] = [
        "name": name,
        "details": details,
        "date_utc": dateString,
        "links": patch,
        "cores": landpadData
    ]
    public static let jsonArrayData: [[String: Any]] = .init(repeating: jsonData, count: 3)
}



