//
//  UIStoryboard+Extension.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    public static func named(_ name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }

    public func instantiate<T: UIViewController>(_ viewControllerType: T.Type) -> T? {
        let className = String(describing: viewControllerType)

        return instantiateViewController(withIdentifier: className) as? T
    }
}
