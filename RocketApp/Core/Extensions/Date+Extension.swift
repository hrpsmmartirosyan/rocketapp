//
//  Date+Extension.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public extension Date {
    func formatToString(format: String, timeZone: TimeZone? = nil) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format

        if let timeZone = timeZone {
            formatter.timeZone = timeZone
        }

        return formatter.string(from: self)
    }
}
