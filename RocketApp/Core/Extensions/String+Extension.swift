//
//  String+Extension.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, comment: "")
    }

    func date(withFormat format: String, inTimeZone timeZone: TimeZone) -> Date? {
        let formatter = DateFormatter()
        formatter.locale = NSLocale.current
        formatter.dateFormat = format
        formatter.timeZone = timeZone

        return formatter.date(from: self)
    }

     func getDate(for dateString: String?) -> String? {
        guard let dateString = dateString else { return nil }

        if let timeZone = TimeZone(abbreviation: "GMT"), let date = dateString.date(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", inTimeZone: timeZone) {
            return date.formatToString(format: "yyyy-MM-dd")
        }

        return nil
    }
}
