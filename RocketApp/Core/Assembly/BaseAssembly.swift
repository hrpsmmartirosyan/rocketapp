//
//  BaseAssembly.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import Swinject

class BaseAssembly: Assembly {
    init() {}

    func assemble(container: Swinject.Container) {}
}
