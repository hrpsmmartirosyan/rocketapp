//
//  RocketDetailViewModel.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

final class RocketDetailInputs {
    let landingService: LandingServicing
    let downloadManagerService: DownloadManagerServicing

    public init(landingService: LandingServicing, downloadManagerService: DownloadManagerServicing) {
        self.landingService = landingService
        self.downloadManagerService = downloadManagerService
    }
}

final class RocketDetailViewModel: BaseViewModel<RocketDetailInputs> {
    public var detailsData = BehaviorRelay<[DetailItemPresentationModel]?>(value: nil)
    public var imageUrl: String?

    func setup(with rocket: Rocket) {
        var models = [DetailItemPresentationModel]()

        imageUrl = rocket.patch?.largeImageUrl
        models.append(DetailItemPresentationModel(name: "rocket.name".localized, value: rocket.name))

        if let fireDate = rocket.dateString {
            let formatedDate = fireDate.getDate(for: fireDate)
            models.append(DetailItemPresentationModel(name: "rocket.fire.date".localized, value: formatedDate))
        }

        if let details = rocket.details {
            models.append(DetailItemPresentationModel(name: "rocket.detail".localized, value: details))
        }

        if let launchpadId = rocket.landpadData?.first?.landpad {
            fetchLaunchpadData(with: launchpadId)
        }

        detailsData.accept(models)
    }

    private func fetchLaunchpadData(with launchpadId: String) {
        let parameters = LandingParameters(landingId: launchpadId)

        self.inputs.landingService
            .call(with: parameters)
            .subscribe { [weak self] event in
                guard let `self` = self else { return }

                self.handleEvent(event) { [weak self] data in
                    guard let `self` = self else { return }

                    var models = self.detailsData.value
                    let locationInfo = (data.region ?? "") + " , " + data.locality
                    let locationData = DetailItemPresentationModel(name: "rocket.location.title".localized, value: locationInfo)

                    models?.append(locationData)

                    self.detailsData.accept(models)
                }
        }
        .disposed(by: disposeBag)

    }
}


