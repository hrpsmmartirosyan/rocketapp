//
//  RocketDetailViewController.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import UIKit
import RxSwift

final class RocketDetailViewController: BaseViewController<RocketDetailViewModel> {
    @IBOutlet private weak var rocketImageView: UIImageView!
    @IBOutlet private weak var detailsStackView: UIStackView!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!

    // MARK: - View life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = "rocket.detail.title".localized

        if let url = viewModel?.imageUrl {

            loadingIndicator.startAnimating()
            viewModel?.inputs.downloadManagerService
                .download(from: URL(string: url)!)
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] data, error in
                    self?.hideIndicator()
                    if error != nil {
                        self?.rocketImageView.image = UIImage(named: "unknown")
                    } else {
                        self?.rocketImageView.image = UIImage(data: data!)
                    }
                    }, onError: { [weak self] error in
                        self?.hideIndicator()
                        self?.rocketImageView.image = UIImage(named: "unknown")
                })
                .disposed(by: disposeBag)
        }
    }

    // MARK: - ViewModel life cycle
    public override func bindViewModel(_ viewModel: RocketDetailViewModel) {
        super.bindViewModel(viewModel)

        viewModel.detailsData
            .asDriver()
            .drive(onNext: { [weak self] detailsData in
                self?.setupStackView(for: detailsData)
            })
            .disposed(by: disposeBag)
    }

    // MARK: - Private methods
    private func setupStackView(for models: [DetailItemPresentationModel]?) {
        guard let models = models else { return }

        for view in self.detailsStackView.subviews {
            if view is DetailItemView {
                view.removeFromSuperview()
            }
        }

        for model in models {
            if let itemView = DetailItemView.loadNib() {
                itemView.translatesAutoresizingMaskIntoConstraints = false
                itemView.setup(with: model)

                self.detailsStackView.addArrangedSubview(itemView)
            }
        }
    }

    private func hideIndicator() {
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true

    }
}

