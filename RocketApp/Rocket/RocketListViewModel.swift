//
//  RocketListViewModel.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import RxSwift
import RxCocoa

final class RocketListInputs {
    let rocketListService: RocketListServicing
    let downloadManagerService: DownloadManagerServicing

    public init(rocketListService: RocketListServicing, downloadManagerService: DownloadManagerServicing) {
        self.rocketListService = rocketListService
        self.downloadManagerService = downloadManagerService
    }
}

final class RocketListViewModel: BaseViewModel<RocketListInputs> {
    // MARK: Public members
    public var datasource = BehaviorRelay<[RocketCellPresentationModel]>(value: [])

    // MARK: Private members
    private var rocketList: [Rocket]?

    var downloadManager: DownloadManagerServicing { return self.inputs.downloadManagerService }

    //MARK: Public API
    func fetchRocketList() {
        let parameters = RocketListParameters()

        self.inputs.rocketListService
            .call(with: parameters)
            .subscribe { [weak self] event in
                guard let `self` = self else { return }

                self.handleEvent(event) { [weak self] list in
                    guard let `self` = self else { return }

                    self.rocketList = list
                    let rocketList = list.map { RocketCellPresentationModel(name: $0.name, dateString: self.getDateString(for: $0.dateString), imageUrl: $0.patch?.smallImageUrl) }

                    self.datasource.accept(rocketList)
                }
            }
            .disposed(by: disposeBag)
    }

    func getSelectedRocket(for index: Int) -> Rocket? {
        return rocketList?[index]
    }

    private func getDateString(for dateString: String?) -> String? {
        guard let string = dateString, let formatedDate = string.getDate(for: string)else { return nil }

        return "rocket.fire.date".localized + formatedDate
    }
}

