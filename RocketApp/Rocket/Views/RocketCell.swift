//
//  RocketCell.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import UIKit
import RxSwift

final class RocketCell: UITableViewCell {
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var rocketImage: UIImageView!

    var model: RocketCellPresentationModel?
    var downloadManager: DownloadManagerServicing!

    var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()

        if let url = model?.imageUrl {
            downloadManager.cancelRequest(of: URL(string: url)!)
        }

        disposeBag = DisposeBag()
    }

    func setup(with model: RocketCellPresentationModel, downloadManager: DownloadManagerServicing) {
        self.model = model
        self.downloadManager = downloadManager
        self.name.text = model.name
        self.dateLabel.text = model.dateString
        self.rocketImage.image = UIImage(named: "unknown")

        if let url = model.imageUrl  {
            downloadManager.download(from: URL(string: url)!)
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] data, error in
                    if error != nil {
                        self?.rocketImage.image = UIImage(named: "unknown")
                    } else {
                        self?.rocketImage.image = UIImage(data: data!)
                    }
                    }, onError: { [weak self] error in
                        self?.rocketImage.image = UIImage(named: "unknown")
                })
                .disposed(by: disposeBag)
        }
    }
}

