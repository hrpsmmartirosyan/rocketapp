//
//  RocketDetailPresentationModel.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

struct RocketDetailPresentationModel {
    let imageUrl: String?
    let itemModels: [DetailItemPresentationModel]?
}
