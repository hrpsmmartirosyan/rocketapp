//
//  RocketCellPresentationModel.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

struct RocketCellPresentationModel {
    let name: String?
    let dateString: String?
    let imageUrl: String?
}
