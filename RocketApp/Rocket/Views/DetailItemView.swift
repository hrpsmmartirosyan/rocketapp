//
//  DetailItemView.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import UIKit

final class DetailItemView: UIView {
    @IBOutlet private weak var itemName: UILabel!
    @IBOutlet private weak var itemValue: UILabel!

    func setup(with model: DetailItemPresentationModel) {
        itemName.text = model.name
        itemValue.text = model.value
    }
}
