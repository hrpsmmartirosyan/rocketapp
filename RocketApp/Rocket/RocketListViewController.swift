//
//  RocketListViewController.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import UIKit
import RxSwift

final class RocketListViewController: BaseViewController<RocketListViewModel>, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var rocketLisTableView: UITableView!

    // MARK: Private members
    private var activityIndicator: UIActivityIndicatorView!
    //As server invalidating cash data each 20 sec's i'm updating data with this interval
    private let serviceUpdatingDelay = 21.0

    // MARK: - View life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = "rocket.list.title".localized
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.setupTableView()
        self.setupLoader()

        self.getRocketData()
    }

    // MARK: - ViewModel life cycle
    public override func bindViewModel(_ viewModel: RocketListViewModel) {
        super.bindViewModel(viewModel)

        viewModel.datasource
            .asDriver()
            .drive(onNext: { [weak self] datasource in
                if !datasource.isEmpty {
                    self?.rocketLisTableView.reloadData()
                    self?.showLoading(false)
                }
            })
            .disposed(by: disposeBag)
    }

    // MARK: - TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datasourc = self.viewModel?.datasource.value else  { return 0 }

        return datasourc.count
    }

    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RocketCell", for: indexPath)

        guard let cellModel = viewModel?.datasource.value[indexPath.row] else { return UITableViewCell() }

        if let cell = cell as? RocketCell, let viewModel = viewModel {
            cell.setup(with: cellModel, downloadManager: viewModel.downloadManager)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openRocketDetail(for: indexPath.row)
    }

    // MARK: Private helpers
    private func setupTableView() {
        rocketLisTableView.register(UINib(nibName: "RocketCell", bundle: nil), forCellReuseIdentifier: "RocketCell")
    }

    private func getRocketData() {
        self.viewModel?.fetchRocketList()

        DispatchQueue.main.asyncAfter(deadline: .now() + serviceUpdatingDelay) {
            self.getRocketData()
        }
    }

    private func setupLoader() {
        activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)

        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }

    private func showLoading(_ show: Bool) {
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }

    private func openRocketDetail(for index: Int) {
        guard let rocketDetailVC =  UIStoryboard.named("Main").instantiate(RocketDetailViewController.self) else { return }

        if let rocket = viewModel?.getSelectedRocket(for: index) {
            rocketDetailVC.viewModel?.setup(with: rocket)
        }

        self.navigationController?.pushViewController(rocketDetailVC, animated: true)
    }
}
