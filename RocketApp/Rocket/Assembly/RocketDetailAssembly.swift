//
//  RocketDetailAssembly.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

final class RocketDetailAssembly: BaseAssembly {
    let transfersAssemblies: [Assembly] = [LandingServiceAssembly()]

    override func assemble(container: Container) {
        self.transfersAssemblies.forEach({ $0.assemble(container: container) })

        container.register(RocketDetailInputs.self) { r in
            let inputs = RocketDetailInputs(landingService: r.resolve(LandingServicing.self)!, downloadManagerService: DownloadManager())

            return inputs
        }

        container.autoregister(RocketDetailViewModel.self, initializer: RocketDetailViewModel.init)

        container.storyboardInitCompleted(RocketDetailViewController.self) { r, c in
            c.viewModel = r.resolve(RocketDetailViewModel.self)
        }
    }
}
