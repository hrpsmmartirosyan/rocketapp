//
//  SwinjectStoryboard+Extension.swift
//  RocketApp
//
//  Created by hripsimem on 1/17/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {

    @objc class func setup() {
        Container.loggingFunction = nil

        //Controllers
        RocketListAssembly().assemble(container: defaultContainer)
        RocketDetailAssembly().assemble(container: defaultContainer)
    }
}
