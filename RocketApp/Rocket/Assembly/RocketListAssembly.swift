//
//  RocketListAssembly.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

final class RocketListAssembly: BaseAssembly {
    let transfersAssemblies: [Assembly] = [RocketListServiceAssembly()]

    override func assemble(container: Container) {
        self.transfersAssemblies.forEach({ $0.assemble(container: container) })

        container.register(RocketListInputs.self) { r in
            let inputs = RocketListInputs(rocketListService: r.resolve(RocketListServicing.self)!, downloadManagerService: DownloadManager())

            return inputs
        }

        container.autoregister(RocketListViewModel.self, initializer: RocketListViewModel.init)

        container.storyboardInitCompleted(RocketListViewController.self) { r, c in
            c.viewModel = r.resolve(RocketListViewModel.self)
        }
    }
}
