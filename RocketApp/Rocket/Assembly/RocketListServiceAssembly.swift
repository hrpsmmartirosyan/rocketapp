//
//  RocketListServiceAssembly.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import Swinject

open class RocketListServiceAssembly: Assembly {
    public init() {}

    open func assemble(container: Container) {
        container.register(RocketListServicing.self) { r in

            container.autoregister(NetworkManager.self, initializer: NetworkManager.init)
            container.autoregister(Parser.self, initializer: Parser.init)

            return RocketListWebService(with: r.resolve(NetworkManager.self)!, parser: r.resolve((Parser.self))!)
        }
    }
}
