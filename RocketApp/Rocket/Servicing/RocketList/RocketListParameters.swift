//
//  RocketListParameters.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public struct RocketListParameters {
    public init() {}
}

extension RocketListParameters: Routing {
    var method: RequestType {
        return .GET
    }

    var encoding: ParameterEncoding {
        return .json
    }

    var routPath: String {
        return "launches"
    }
}
