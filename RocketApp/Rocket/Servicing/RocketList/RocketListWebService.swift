//
//  RocketListWebService.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

// RocketListWebService is crated for making real service call
// If we will need to have mockes too we can have RocketListMockService
// Which will be injected if we need to have mockes

class RocketListWebService: DataFetchingManager, RocketListServicing {
    func call(with parameters: RocketListParameters) -> Observable<[Rocket]> {
        return self.execute(parameters, errorType: ErrorResponse.self)
    }
}
