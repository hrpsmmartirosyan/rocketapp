//
//  RocketListServicing.swift
//  RocketApp
//
//  Created by hripsimem on 1/16/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

public protocol RocketListServicing {
    /**
     Get rocket information

     - returns:  returned Rocket list, or throws error via Observable
     */
    func call(with parameters: RocketListParameters) -> Observable<[Rocket]>
}

