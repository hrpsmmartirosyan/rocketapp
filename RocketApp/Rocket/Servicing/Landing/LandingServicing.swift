//
//  landPadsServicing.swift
//  RocketApp
//
//  Created by hripsimem on 1/18/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

public protocol LandingServicing {
    /**
     Get landing information

     - returns:  returned Landing data, or throws error via Observable
     */
    func call(with parameters: LandingParameters) -> Observable<Landing>
}
