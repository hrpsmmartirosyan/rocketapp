//
//  LandingWebService.swift
//  RocketApp
//
//  Created by hripsimem on 1/18/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

class LandingWebService: DataFetchingManager, LandingServicing {
    func call(with parameters: LandingParameters) -> Observable<Landing> {
        return self.execute(parameters, errorType: ErrorResponse.self)
    }
}

