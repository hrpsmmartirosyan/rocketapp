//
//  LandingParameters.swift
//  RocketApp
//
//  Created by hripsimem on 1/18/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import Foundation

public struct LandingParameters {
    public let landingId: String

    public init(landingId: String) {
        self.landingId = landingId
    }
}

extension LandingParameters: Routing {
    var method: RequestType {
        return .GET
    }

    var pathValues: [String]? {
        [landingId]
    }

    var encoding: ParameterEncoding {
        return .json
    }

    var routPath: String {
        return "landpads"
    }
}
