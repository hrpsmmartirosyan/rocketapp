//
//  PatchEntityTest.swift
//  RocketAppTests
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import XCTest
@testable import RocketApp
import Fakery

class PatchEntityTest: XCTestCase {
    private static let faker = Faker()

    let jsonDecoder: JSONDecoder = JSONDecoder()

        func test_initialization_withAllProperties() {
            // Arrange: Create instance of Patch with all properties
            let object = Patch(smallImageUrl: PatchEntityTest.faker.lorem.word(), largeImageUrl: PatchEntityTest.faker.lorem.word())

            // Assert: All parameters have correct values
            XCTAssertEqual(object.smallImageUrl, PatchEntityTest.faker.lorem.word())
            XCTAssertEqual(object.largeImageUrl, PatchEntityTest.faker.lorem.word())
        }

        var validJson: Data {
            return PatchFactory.createTestableData()
        }

        func test_decoding_withAllProperties() {
            // Act: Decode object of type Patch with valid json containing all properties using Decodable
            do {
                let object = try jsonDecoder.decode(Patch.self, from: validJson)

                // Assert: All parameters have correct values
                XCTAssertEqual(object.smallImageUrl, PatchFactory.smallImageUrl)
                XCTAssertEqual(object.largeImageUrl, PatchFactory.largeImageUrl)

            } catch {
                XCTFail("couldn't decode : \(error)")
            }
        }

    func test_initialization_withoutOptionalPropertySmallImageUrl() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Patch(smallImageUrl: nil, largeImageUrl: PatchFactory.largeImageUrl)
        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.smallImageUrl)
        XCTAssertEqual(object.largeImageUrl, PatchFactory.largeImageUrl)
    }

    func test_initialization_withoutOptionalPropertyLargImageUrl() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Patch(smallImageUrl: PatchFactory.smallImageUrl, largeImageUrl: nil)

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.largeImageUrl)
        XCTAssertEqual(object.smallImageUrl, PatchFactory.smallImageUrl)
    }
}

