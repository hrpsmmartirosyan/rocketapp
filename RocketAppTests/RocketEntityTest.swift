//
//  RocketEntityTest.swift
//  RocketAppTests
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import XCTest
@testable import RocketApp
import Fakery

class RocketEntityTest: XCTestCase {
    private static let faker = Faker()

    let jsonDecoder: JSONDecoder = JSONDecoder()

    func test_initialization_withAllProperties() {
        // Arrange: Create instance of Rocket with all properties
        let object = Rocket(name: RocketEntityTest.faker.lorem.word(), dateString: RocketEntityTest.faker.lorem.word(), details: RocketEntityTest.faker.lorem.word(), patch: PatchFactory.jsonObject, landpadData: [LandpadDataFactory.jsonObject])

        // Assert: All parameters have correct values
        XCTAssertEqual(object.name, RocketEntityTest.faker.lorem.word())
        XCTAssertEqual(object.dateString, RocketEntityTest.faker.lorem.word())
        XCTAssertEqual(object.details, RocketEntityTest.faker.lorem.word())
        XCTAssertEqual(object.patch, PatchFactory.jsonObject)
        XCTAssertEqual(object.landpadData, [LandpadDataFactory.jsonObject])
    }

    var validJson: Data {
        return RocketFactory.createTestableData()
    }

    func test_decoding_withAllProperties() {
        // Act: Decode object of type RocketData with valid json containing all properties using Decodable
        do {
            let object = try jsonDecoder.decode(Rocket.self, from: validJson)

            // Assert: All parameters have correct values
            XCTAssertEqual(object.name, RocketFactory.name)
            XCTAssertEqual(object.dateString, RocketFactory.dateString)
            XCTAssertEqual(object.details, RocketFactory.details)
            XCTAssertEqual(object.landpadData, [LandpadDataFactory.jsonObject])

        } catch {
            XCTFail("couldn't decode : \(error)")
        }
    }


    func test_initialization_withoutOptionalPropertyDateString() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Rocket(name: RocketFactory.name, dateString: nil, details: RocketFactory.dateString, patch: PatchFactory.jsonObject, landpadData: [LandpadDataFactory.jsonObject])

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.dateString)
        XCTAssertEqual(object.name, RocketFactory.name)
        XCTAssertEqual(object.details, RocketFactory.details)
        XCTAssertEqual(object.patch, PatchFactory.jsonObject)
        XCTAssertEqual(object.landpadData, [LandpadDataFactory.jsonObject])
    }

    func test_initialization_withoutOptionalPropertyDetails() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Rocket(name: RocketFactory.name, dateString: RocketFactory.dateString, details: nil, patch: PatchFactory.jsonObject, landpadData: [LandpadDataFactory.jsonObject])

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.details)
        XCTAssertEqual(object.name, RocketFactory.name)
        XCTAssertEqual(object.dateString, RocketFactory.dateString)
        XCTAssertEqual(object.patch, PatchFactory.jsonObject)
        XCTAssertEqual(object.landpadData, [LandpadDataFactory.jsonObject])
    }

    func test_initialization_withoutOptionalPropertyPatch() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Rocket(name: RocketFactory.name, dateString: RocketFactory.dateString, details: RocketFactory.dateString, patch: nil, landpadData: [LandpadDataFactory.jsonObject])

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.patch)
        XCTAssertEqual(object.name, RocketFactory.name)
        XCTAssertEqual(object.dateString, RocketFactory.dateString)
        XCTAssertEqual(object.details, RocketFactory.details)
        XCTAssertEqual(object.landpadData, [LandpadDataFactory.jsonObject])
    }

    func test_initialization_withoutOptionalPropertyLandpadData() {
        // Arrange: Create instance of Patch with all properties except agencies
        let object = Rocket(name: RocketFactory.name, dateString: RocketFactory.dateString, details: RocketFactory.details, patch: PatchFactory.jsonObject, landpadData: nil)

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.landpadData)
        XCTAssertEqual(object.name, RocketFactory.name)
        XCTAssertEqual(object.dateString, RocketFactory.dateString)
        XCTAssertEqual(object.details, RocketFactory.details)
        XCTAssertEqual(object.patch, PatchFactory.jsonObject)
    }

    var invalidJsonWithoutName: Data {
        return RocketFactory.createInvalid(name: true)
    }

    func test_decoding_withoutRequiredPropertyName() {
        // Act: Decode object of type AgencyAddress with invalid json containing all properties except required address using Decodable
        do {
            _ = try jsonDecoder.decode(Rocket.self, from: invalidJsonWithoutName)

            // Assert: Object should be nil
            XCTFail("Object AgencyAddress should be nil")
        } catch {
        }
    }
}
