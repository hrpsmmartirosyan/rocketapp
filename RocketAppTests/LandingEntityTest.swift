//
//  LandingEntityTest.swift
//  RocketAppTests
//
//  Created by hripsimem on 1/19/21.
//  Copyright © 2021 hripsimem. All rights reserved.
//

import XCTest
@testable import RocketApp
import Fakery

class LandingEntityTest: XCTestCase {
    private static let faker = Faker()

    let jsonDecoder: JSONDecoder = JSONDecoder()

    func test_initialization_withAllProperties() {
        // Arrange: Create instance of LandingData with all properties
        let object = LandpadData(landpad: LandingEntityTest.faker.lorem.word())

        // Assert: All parameters have correct values
        XCTAssertEqual(object.landpad, LandingEntityTest.faker.lorem.word())
    }

    var validJson: Data {
        return LandpadDataFactory.createTestableData()
    }

    func test_decoding_withAllProperties() {
        // Act: Decode object of type LandingData with valid json containing all properties using Decodable
        do {
            let object = try jsonDecoder.decode(LandpadData.self, from: validJson)

            // Assert: All parameters have correct values
            XCTAssertEqual(object.landpad, LandpadDataFactory.landpad)

        } catch {
            XCTFail("couldn't decode : \(error)")
        }
    }

    func test_initialization_withoutOptionalPropertySmallImageUrl() {
        // Arrange: Create instance of LandingData with all properties except agencies
        let object = LandpadData(landpad: nil)

        // Assert: All parameters have correct values and agencies is nil
        XCTAssertNil(object.landpad)
    }
}


